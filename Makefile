ifeq ($(SHELL), sh.exe)
	CMD_DEL = del /F /Q
else
	CMD_DEL = rm -f
endif
all: bin/md5_breaker
	@echo Done
bin/md5_breaker: bin/ obj/release/ obj/release/dico_attac.o obj/release/main.o obj/release/md5.o
	@echo Compilation de bin/md5_breaker
	/usr/bin/g++ -fopenmp -s -Wl,--gc-sections -Wl,-O1 -o bin/md5_breaker obj/release/dico_attac.o obj/release/main.o obj/release/md5.o -lm
bin:
	@echo Création du dossier: bin/
	@mkdir bin
obj/release:
	@echo Création du dossier: obj/release/
	@mkdir obj
	@mkdir obj/release
obj/release/dico_attac.o: src/dico_attac.cpp src/dico_attac.h src/md5.h src/macro.h
	@echo Compilation de src/dico_attac.cpp
	/usr/bin/g++   -std=c++11 -std=gnu++11 -fopenmp -W -Wall -Wextra -Wdeprecated -Wuninitialized -Wunused-variable -Wvariadic-macros -Wredundant-decls -Wunused-macros -Wundef -Wfloat-equal -Wwrite-strings -Wredundant-decls -Winit-self -Wpointer-arith -Wcast-align -Os -fdata-sections -ffunction-sections -c -pipe src/dico_attac.cpp -o obj/release/dico_attac.o
obj/release/main.o: src/main.cpp src/dico_attac.h src/macro.h
	@echo Compilation de src/main.cpp
	/usr/bin/g++   -std=c++11 -std=gnu++11 -fopenmp -W -Wall -Wextra -Wdeprecated -Wuninitialized -Wunused-variable -Wvariadic-macros -Wredundant-decls -Wunused-macros -Wundef -Wfloat-equal -Wwrite-strings -Wredundant-decls -Winit-self -Wpointer-arith -Wcast-align -Os -fdata-sections -ffunction-sections -c -pipe src/main.cpp -o obj/release/main.o
obj/release/md5.o: src/md5.cpp src/md5.h
	@echo Compilation de src/md5.cpp
	/usr/bin/g++   -std=c++11 -std=gnu++11 -fopenmp -W -Wall -Wextra -Wdeprecated -Wuninitialized -Wunused-variable -Wvariadic-macros -Wredundant-decls -Wunused-macros -Wundef -Wfloat-equal -Wwrite-strings -Wredundant-decls -Winit-self -Wpointer-arith -Wcast-align -Os -fdata-sections -ffunction-sections -c -pipe src/md5.cpp -o obj/release/md5.o
clean:
	@echo Suppression des fichiers temporaires
	$(CMD_DEL) obj/release/dico_attac.o obj/release/main.o obj/release/md5.o
exec: bin/md5_breaker
	cd bin/ && ./md5_breaker
