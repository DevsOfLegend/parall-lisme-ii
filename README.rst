======
README
======

:Info: See <https://bitbucket.org/DevsOfLegend/parall-lisme-ii> for repo.
:Author: NUEL Guillaume (ImmortalPC) and DECK Léa (dla)
:Date: $Date: 2013-05-23 $
:Revision: $Revision: 10 $
:Description: Projet pour le cours de Master 1 de Cryptis: Parallélisme 2. Le projet concerne un Système de crackage de MD5 avec OpenMP.


**Description**
---------------
Projet pour le cours de Master 1 de Cryptis: Parallélisme 2. Le projet concerne un Système de crackage de MD5 avec OpenMP.

|
| Ce programme comprend les fonctionnalité suivantes:

- Adaptation au nombre de Core (OpenMP inside) (Scalability)
- Paramétrable via la ligne de commande


**Format** **du** **code**
--------------------------
- Le code est en UTF-8
- Le code est indenté avec des tabulations réel (\\t)
- Le code est prévus pour être affiché avec une taille de 4 pour les tab (\\t)
- Les fins de lignes sont de type LF (\\n)
- IDE utilisé: QtCreator


**Outils nécessaires**
----------------------
- G++ avec support d'OpenMP


**Normes**
----------
- Les commentaires sont au format doxygen.


**Utilisation**
---------------
>>> ./main -dbpass=password_file.txt -nbWordToUse=3 -file=hacked1.txt,hacked2.txt,...

-dbpass		: Fichier de mot de passe
-nbWordToUse	: Nombre de mot de passes utilisé pour trouver l'iteration magique
-file		: Liste des fichiers contenant les md5 à craquer.

.. Attention::
	Les fichiers donnés DOIVENT avoir le format: 1 password/md5 par ligne


**Documentation**
-----------------
| Pour plus d'informations voir la documentation dans le dossier **/doc/**
| Énoncer du projet: **/doc/Parallelisme_II_TP_2012_2013_Projet_classique.pdf**
| Rapport du projet; **/doc/rapport.pdf**


**IDE** **RST**
---------------
RST créé grâce au visualiseur: https://github.com/anru/rsted
