#include "dico_attac.h"
#include "macro.h"
#include <vector>
#include <string>
#include <string.h>
#include <fstream>
#include <time.h>

using namespace std;

// Voir plus bas ;-)
void help();
bool callback( int8_t iter, const std::string& md5Hexa, const std::string& password, std::vector<int8_t>& iterResult, const std::vector< std::vector< std::string > >& md5_to_break );
vector<string> md5_file;//!< Liste des fichier md5 (utilisé dans le callback)
uint32_t nbPasswordToFind = 0;//!< Nombre de password retant a trouver (utilisé dans le callback)


/***************************************************************************//*!
 * @brief Main
 * @param[in] argc		Nombre d'argument
 * @param[in] argv		Liste des arguments
 * @return 0 Si tout s'est bien passé.
 *
 * NOTE : command line
 *	./main -dbpass=password_file.txt -nbWordToUse=3 -file=hacked1.txt,hacked2.txt,...
 *
 *	-> -dbpass		 : Fichier de mot de passe
 *	-> -nbWordToUse	 : Nombre de mot de passes utilisé pour trouver l'iteration magique
 *	-> -file		 : Liste des fichiers contenant les md5 à craquer.
 *	-> -useFinalCache: Utiliser la version avec cache lors de la recherche du passe admin ?
 *
 * @warning Les fichiers donnés DOIVENT avoir le format: 1 password/md5 par ligne
 */
int main (int argc, char** argv)
{
	int nbWordToUse = 3;//!< Nombre de mot de passe a utiliser
	uint8_t nbMaxIter = 5;//!< Nombre maxi d'iteration
	time_t begin = 0;// Permet de déterminer des stats
	time_t totalTime = time(0);// Permet de déterminer des stats
	uint8_t nbFile = 5;
	const char* md5_file_default[5] = {
		"system_1.phl",
		"system_2.phl",
		"system_3.phl",
		"system_4.phl",
		"system_5.phl",
	};
	const char dicoFile_default[] = "high_frequency_passwords_list.txt";
	bool useFinalCache = false;


	string dicoFile;


	/***********************************************************************//*!
	* Ligne de commande
	*/
	if( argc > 1 ){
		for( int i=1; i<argc; i++ )
		{
			if( strncmp(argv[i], "-d", 2) == 0 ){
				dicoFile = string(argv[i]);
				if( dicoFile.size() <= 8 ){
					printf("Commande incorrect: <%s>\n", argv[i]);
					help();
				}
				dicoFile = dicoFile.substr(8);

			}else if( strncmp(argv[i], "-n", 2) == 0 ){
				string tmp(argv[i]);
				if( tmp.size() <= 13 ){
					printf("Commande incorrect: <%s>\n", argv[i]);
					help();
				}
				if( sscanf(tmp.substr(13).c_str(), "%u", &nbWordToUse) == 0 ){
					printf("Commande incorrect: <%s>\n", argv[i]);
					help();
				}

			}else if( strncmp(argv[i], "-f", 2) == 0 ){
				string tmp = string(argv[i]).substr(6);
				if( tmp.size() <= 6 ){
					printf("Commande incorrect: <%s>\n", argv[i]);
					help();
				}

				uint posStart = 0;
				uint posEnd = tmp.find(",");

				if( posEnd == string::npos ){
					md5_file.push_back(tmp);

				}else{
					while( 1 )
					{
						posEnd = tmp.find(",", posStart);
						if( posEnd == string::npos ){
							md5_file.push_back(tmp.substr(posStart));
							break;
						}
						md5_file.push_back(tmp.substr(posStart, posEnd));
						posStart = posEnd+1;
					}
				}
				nbFile = md5_file.size();

			}else if( strncmp(argv[i], "-u", 2) == 0 ){
				useFinalCache = true;

			}else{
				printf("Commande incorrect: <%s>\n", argv[i]);
				help();
			}
		}
	}


	/***********************************************************************//*!
	* Paramètres par défaut
	*/
	if( !md5_file.size() ){
		for( uint8_t i=0; i<nbFile; i++ )
			md5_file.push_back(md5_file_default[i]);
	}
	if( !dicoFile.size() )
		dicoFile = dicoFile_default;



	/***********************************************************************//*!
	* Lecture du dico
	*/
	vector< string > dico;
	{
		ifstream fp(dicoFile);
		if( !fp.is_open() )
			stdErrorE("Erreur lors de la lecture du fichier <%s>", dicoFile.c_str());

		string line;
		while( std::getline(fp, line) ){
			if( line.size() == 0 )
				continue;
			dico.push_back(line);
		}
	}
	if( nbWordToUse == -1 || dico.size() < (uint32_t)nbWordToUse )
		nbWordToUse = dico.size();

	vector< vector< string > > md5_to_break;//!< Contient la liste de TOUT les md5 a casser
	md5_to_break.resize(nbFile);


	/***********************************************************************//*!
	* Lecture des fichiers
	*/
	#pragma omp parallel for shared(md5_to_break)
	for( uint8_t Ifile=0; Ifile<nbFile; Ifile++ )
	{
		ifstream fp(md5_file[Ifile]);
		if( !fp.is_open() )
			stdErrorE("Erreur lors de la lecture du fichier <%s>", md5_file[Ifile].c_str());

		md5_to_break[Ifile].reserve(10);// On suppose qu'il y a au moins 10 md5

		string line;
		while( std::getline(fp, line) )
		{
			if( line.size() != 32 )
				break;
			md5_to_break[Ifile].push_back(line);
		}
	}

	// Allocation du cache
	uint32_t cacheSize = nbWordToUse*nbMaxIter;
	md5Cache_t* md5Cache = 0;
	md5Cache = genCache(dico, nbMaxIter, 0, nbWordToUse);

	// On stock les itérations dans cette variable
	vector<int8_t> iterResult;
	iterResult.resize(nbFile, -1);


	/***********************************************************************//*!
	* On cherche activement les iterations magique
	*/
	begin = time(0);

	#pragma omp parallel for shared(md5Cache,iterResult)
	for( uint32_t Ifile=0; Ifile<nbFile; Ifile++ )
	{
		#pragma omp parallel for shared(md5Cache,iterResult)
		for( uint32_t Imd5=0; Imd5<md5_to_break[Ifile].size(); Imd5++ )// md5_to_break[Ifile][Imd5][0]
		{
			#pragma omp parallel for shared(md5Cache,iterResult)
			for( uint32_t iCache=0; iCache<cacheSize; iCache++ )
			{
				if( iterResult[Ifile] == -1 && (*md5Cache)[iCache] == md5_to_break[Ifile][Imd5] ){
					iterResult[Ifile] = iCache%nbMaxIter;
					int wi = (iCache-iterResult[Ifile])/nbMaxIter;
					printf("md5_file=%s, nbIter=%d, %s -> %s\n", md5_file[Ifile].c_str(), iterResult[Ifile]+1, md5_to_break[Ifile][Imd5].c_str(), dico[wi].c_str());
				}
			}
		}
	}
	printf("Iterations trouvee en %ld sec\n", time(0)-begin);
	delete md5Cache;


	/***************************************************************************
	* On a trouvé les itérations !
	* Mais on a pas trouvé les password admin
	* On sait que le password admin n'est pas dans les {nbWordToUse} (on vient des les faire)
	* Donc on va générer un cache de tout sauf ça
	*/

	// Version 1 avec callback
	if( !useFinalCache ){
		nbPasswordToFind = md5_to_break.size();
		execCacheByTab( dico, nbWordToUse, iterResult, md5_to_break, callback);
		printf("Total time %ld secs\n", time(0)-totalTime);
		return 0;
	}

	// Version 2 sans callback => résultat uniquement quand le cache est plein
	cacheRestricted_t* cacheR = genCacheByTab( dico, nbWordToUse, iterResult );

	vector<bool> found;
	found.resize(nbFile, false);

	begin = time(0);
	#pragma omp parallel for
	for( uint32_t Ifile=0; Ifile<nbFile; Ifile++ )
	{
		md5Cache_t& md5_cache = (*cacheR)[iterResult[Ifile]];// On optimize l'accès
		#pragma omp parallel for
		for( uint32_t iCache=0; iCache<md5_cache.size(); iCache++ )
		{
			if( md5_cache[iCache] == md5_to_break[Ifile][0] ){
				printf("Admin password=%s, md5_file=%s, nbIter=%d\n", dico[iCache+nbWordToUse].c_str(), md5_file[Ifile].c_str(), iterResult[Ifile]+1);
				found[Ifile] = true;
			}
		}
		if( found[Ifile] == false )
			printf("Admin password not found for md5_file=%s\n", md5_file[Ifile].c_str());
	}
	printf("Password trouve en %ld sec\n", time(0)-begin);
	delete cacheR;

	printf("Total time %ld secs\n", time(0)-totalTime);

	return 0;
}


/***************************************************************************//*!
* @brief Affiche l'aide
* @return[NONE]
*/
void help()
{
	printf("MD5 Cracker\n");
	printf("Usage:\n");
	printf("./main [-dbpass=password_file.txt] [-nbWordToUse=3] [-file=hacked1.txt,hacked2.txt,...]\n");
	printf("\n");
	printf("	%-20s Fichier de mot de passe\n", "-dbpass:");
	printf("	%-20s Nombre de mot de passes utilisé pour trouver l'iteration magique\n", "-nbWordToUse:");
	printf("	%-20s Liste des fichiers contenant les md5 à craquer.\n", "-file:");
	printf("	%-20s Utiliser la version avec cache lors de la recherche du passe admin ?\n", "-useFinalCache:");
	printf("\n");
	printf("Attention !\n");
	printf("	Les fichiers donnés DOIVENT avoir le format: 1 password (OU md5) par ligne !\n");
	exit(0);
}


/***************************************************************************//*!
* @brief Permet de comparer un md5hexa avec un password admin
* @return TRUE si execCacheByTab doit continuer a chercher. False pour stopper la recherche
*/
bool callback( int8_t iter, const std::string& md5Hexa, const std::string& password, std::vector<int8_t>& iterResult, const std::vector< std::vector< std::string > >& md5_to_break )
{
	#pragma omp parallel for shared(nbPasswordToFind)
	for( uint32_t Ifile=0; Ifile<md5_file.size(); Ifile++ )
	{
		if( Ifile<md5_file.size() && iterResult[Ifile] == iter && md5Hexa == md5_to_break[Ifile][0] ){
			printf("Admin password=%s, md5_file=%s, nbIter=%d\n", password.c_str(), md5_file[Ifile].c_str(), iter+1);
			#pragma omp critical
			nbPasswordToFind--;
		}
	}
	return nbPasswordToFind != 0;
}
