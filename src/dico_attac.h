#ifndef DICO_ATTAC_h
#define DICO_ATTAC_h
#include <stdint.h>
#include <stdint-gcc.h>
#include <vector>
#include <string>
#include <map>
/***************************************************************************//*!
* @file dico_attac.h
* @brief Les fonctions suivantes permettent de trouver l'association md5 <-> password
* 3 fonctions majeurs:
*	- genCache se charge de générer un cache complet ( toutes les itérations )
*	- genCacheByTab se charge de générer un cache restraint ( renvoie uniquement
*	  les itérations indiquées dans  {lstHashNeed} )
*	- execCacheByTab se charge d'executer un callback a chaque itération de chaque password
*/

typedef std::vector<std::string> md5Cache_t;//!< Type du cache
typedef std::map< uint8_t, md5Cache_t > cacheRestricted_t;//!< Cache réduit
typedef bool (*ptrFunc)(int8_t, const std::string&, const std::string&, std::vector<int8_t>&, const std::vector< std::vector< std::string > >&);
// Iter, md5Hexa, password, iterResult, md5_to_break

md5Cache_t* genCache(
	const std::vector<std::string>& dico,
	uint8_t nbIter,
	uint32_t startDicoAt = 0,
	uint32_t endDicoAt = __UINT32_MAX__
);

cacheRestricted_t* genCacheByTab(
	const std::vector<std::string>& dico,
	uint32_t startDicoAt,
	const std::vector<int8_t>& lstHashNeed
);

void execCacheByTab(
	const std::vector<std::string>& dico,
	uint32_t startDicoAt,
	std::vector<int8_t>& lstHashNeed,
	const std::vector< std::vector< std::string > >& md5_to_break,
	ptrFunc callBackEachIter
);

bool isInList( const std::vector<int8_t>& lst, int8_t find );

#endif// DICO_ATTAC_h
