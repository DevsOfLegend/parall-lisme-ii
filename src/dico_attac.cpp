﻿#include "dico_attac.h"
#include "md5.h"
#include "macro.h"
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <time.h>


/***************************************************************************//*!
* @brief Permet de générer un cache md5 depuis le {dico}.
* @param[in] dico				Le dico a utiliser
* @param[in] nbIter				Nombre d'iteration a appliquer
* @param[in,opt] startDicoAt	Lire le {dico} a partir de ...
* @param[in,opt] endDicoAt		Lire le dico jusqu'a ...
* @return Liste des password en md5 ( un md5 par mot et par itération )
*
* @warning {md5Cache} Doit être de la taille suivante: dicoHash[dicoSize*nbIter][MD5_HASHBYTES*2+1];
*/
md5Cache_t* genCache(
	const std::vector<std::string>& dico,
	uint8_t nbIter,
	uint32_t startDicoAt,
	uint32_t endDicoAt
){
	if( startDicoAt > dico.size() )
		startDicoAt = 0;
	if( endDicoAt > dico.size() )
		endDicoAt = dico.size();
	if( startDicoAt > endDicoAt )
		stdErrorE("Valeur incorrect: startDicoAt=%u, endDicoAt=%u", startDicoAt, endDicoAt);

	// On veut 10 mots de passes en md5 avec 10 itérations possibles
	// le tout en hexa
	//char md5Cache[dicoSize*nbIter][MD5_HASHBYTES*2+1];
	md5Cache_t* md5Cache = new md5Cache_t();
	md5Cache->resize(nbIter*(endDicoAt-startDicoAt));

	// Temp
	unsigned char md5_tmp1[MD5_HASHBYTES] = {0};
	unsigned char md5_tmp2[MD5_HASHBYTES] = {0};

	printf("Gen cache with %u iterations\n", nbIter);
	time_t begin = time(0);

	#pragma omp parallel for shared(md5Cache,dico) private(md5_tmp1,md5_tmp2)
	for( uint32_t IbasicWords=startDicoAt; IbasicWords<endDicoAt; IbasicWords++ )
	{
		printf("~ %-20s -- %d/%d\n", dico[IbasicWords].c_str(), IbasicWords-startDicoAt+1, endDicoAt-startDicoAt);
		char md5Hex[MD5_HASHBYTES*2+1] = {0};

		// Convertion
		calcul_md5((unsigned char*)dico[IbasicWords].c_str(), dico[IbasicWords].size(), md5_tmp1);
		for( int iGen=0; iGen<1000000-1; iGen++ )
		{
			calcul_md5(md5_tmp1, MD5_HASHBYTES, md5_tmp2);
			memcpy(md5_tmp1, md5_tmp2, MD5_HASHBYTES);
		}
		md5_2_hex(md5_tmp1, md5Hex);
		(*md5Cache)[IbasicWords*nbIter+0] = md5Hex;


		for( uint8_t iter=1; iter<nbIter; iter++ )
		{
			for( int iGen=0; iGen<1000000; iGen++ )
			{
				calcul_md5(md5_tmp1, MD5_HASHBYTES, md5_tmp2);
				memcpy(md5_tmp1, md5_tmp2, MD5_HASHBYTES);
			}
			md5_2_hex(md5_tmp1, md5Hex);
			(*md5Cache)[IbasicWords*nbIter+iter] = md5Hex;
		}
	}
	printf("Cache ready in %ld secs !\n", time(0)-begin);
	return md5Cache;
}


/***************************************************************************//*!
* @brief Permet de générer un cache md5 depuis le {dico}.
* @param[in] dico				Le dico a utiliser
* @param[in] startDicoAt		Commencer la lecture du dico appartir du {startDicoAt} element
* @param[in] lstHashNeed		Liste des itérations a retourner
* @return Une liste des md5 généré. FORMAT: {iter: [md5 list]}. Ex: {2: ['bca28cc943c724663bbeb8e6b3658086',...], ...}
*/
cacheRestricted_t* genCacheByTab(
	const std::vector<std::string>& dico,
	uint32_t startDicoAt,
	const std::vector<int8_t>& lstHashNeed
){
	// On veut 10 mots de passes en md5 avec 10 itérations possible
	// le tout en hexa
	//char dicoHash[nbIter][dicoSize][MD5_HASHBYTES*2+1];

	cacheRestricted_t* cache = new cacheRestricted_t();

	// Temp
	unsigned char md5_tmp1[MD5_HASHBYTES] = {0};
	unsigned char md5_tmp2[MD5_HASHBYTES] = {0};
	int8_t nbMaxIter = *(std::max_element(lstHashNeed.begin(), lstHashNeed.end()))+1;

	printf("Gen cache with %u iterations\n", nbMaxIter);
	time_t begin = time(0);

	uint32_t dicoSize = dico.size();

	// On prépare le cache pour eviter d'utiliser #pragma omp critical
	for( int8_t iter=0; iter<nbMaxIter; iter++ )
		(*cache)[iter].resize(dico.size()-startDicoAt);// On ajuste la taille du vector

	#pragma omp parallel for shared(cache, dico) private(md5_tmp1,md5_tmp2)
	for( uint32_t IbasicWords=startDicoAt; IbasicWords<dicoSize; IbasicWords++ )
	{
		printf("~ %-20s -- %d/%d\n", dico[IbasicWords].c_str(), IbasicWords+1-startDicoAt, dicoSize-startDicoAt);
		char md5Hex[MD5_HASHBYTES*2+1] = {0};

		// Convertion
		calcul_md5((unsigned char*)dico[IbasicWords].c_str(), dico[IbasicWords].size(), md5_tmp1);
		for( int iGen=0; iGen<1000000-1; iGen++ )
		{
			calcul_md5(md5_tmp1, MD5_HASHBYTES, md5_tmp2);
			memcpy(md5_tmp1, md5_tmp2, MD5_HASHBYTES);
		}
		// On ne garde que les hash qui nous intéresse
		if( isInList(lstHashNeed, 0) ){
			md5_2_hex(md5_tmp1, md5Hex);
			(*cache)[0][IbasicWords-startDicoAt] = md5Hex;
		}

		// On ne peut pas paralleliser ce for, car on a besoin du i-1
		for( int8_t iter=1; iter<nbMaxIter; iter++ )
		{
			for( int iGen=0; iGen<1000000; iGen++ )
			{
				calcul_md5(md5_tmp1, MD5_HASHBYTES, md5_tmp2);
				memcpy(md5_tmp1, md5_tmp2, MD5_HASHBYTES);
			}
			// On ne garde que les hash qui nous intéresse
			if( isInList(lstHashNeed, iter) ){
				md5_2_hex(md5_tmp1, md5Hex);
				(*cache)[iter][IbasicWords-startDicoAt] = md5Hex;
			}
		}
	}
	printf("Cache ready in %ld secs !\n", time(0)-begin);
	return cache;
}


/***************************************************************************//*!
* @brief Va executer {callBackEachIter} à chaque password à chaque itération.
* @param[in] dico				Le dico a utiliser
* @param[in] startDicoAt		Commencer la lecture du dico appartir du {startDicoAt} element
* @param[in] lstHashNeed		Liste des itérations a retourner
* @param[in] md5_to_break		Liste des md5 a casser ( var passée en arg à callback)
* @return[NONE]
*/
void execCacheByTab(
	const std::vector<std::string>& dico,
	uint32_t startDicoAt,
	std::vector<int8_t>& lstHashNeed,
	const std::vector< std::vector< std::string > >& md5_to_break,
	ptrFunc callBackEachIter
){
	// Temp
	unsigned char md5_tmp1[MD5_HASHBYTES] = {0};
	unsigned char md5_tmp2[MD5_HASHBYTES] = {0};
	int8_t nbMaxIter = *(std::max_element(lstHashNeed.begin(), lstHashNeed.end()))+1;

	printf("Execute and gen cache with %u iterations\n", nbMaxIter);
	time_t begin = time(0);

	uint32_t dicoSize = dico.size();
	bool continueLoop = true;

	#pragma omp parallel for shared(dico,lstHashNeed,md5_to_break) private(md5_tmp1,md5_tmp2)
	for( uint32_t IbasicWords=startDicoAt; IbasicWords<dicoSize; IbasicWords++ )
	{
		printf("~ %-20s -- %d/%d\n", dico[IbasicWords].c_str(), IbasicWords+1-startDicoAt, dicoSize-startDicoAt);
		char md5Hex[MD5_HASHBYTES*2+1] = {0};

		if( !continueLoop )
			continue;

		// Convertion
		calcul_md5((unsigned char*)dico[IbasicWords].c_str(), dico[IbasicWords].size(), md5_tmp1);
		for( int iGen=0; iGen<1000000-1; iGen++ )
		{
			if( !continueLoop )
				break;

			calcul_md5(md5_tmp1, MD5_HASHBYTES, md5_tmp2);
			memcpy(md5_tmp1, md5_tmp2, MD5_HASHBYTES);
		}
		// On ne garde que les hash qui nous intéresse
		if( isInList(lstHashNeed, 0) ){
			md5_2_hex(md5_tmp1, md5Hex);
			continueLoop = callBackEachIter(0, std::string(md5Hex), dico[IbasicWords], lstHashNeed, md5_to_break);
		}

		// On ne peut pas paralleliser ce for, car on a besoin du i-1
		for( int8_t iter=1; iter<nbMaxIter; iter++ )
		{
			for( int iGen=0; iGen<1000000; iGen++ )
			{
				if( !continueLoop )
					break;

				calcul_md5(md5_tmp1, MD5_HASHBYTES, md5_tmp2);
				memcpy(md5_tmp1, md5_tmp2, MD5_HASHBYTES);
			}
			// On ne garde que les hash qui nous intéresse
			if( isInList(lstHashNeed, iter) ){
				md5_2_hex(md5_tmp1, md5Hex);
				continueLoop = callBackEachIter(iter, std::string(md5Hex), dico[IbasicWords], lstHashNeed, md5_to_break);
			}
		}
	}
	printf("Done in %ld secs !\n", time(0)-begin);
}


/***************************************************************************//*!
* @brief Permet de tester l'existance d'un element dans une liste
* @param[in] lst	Liste de recherche
* @param[in] find	Element a rechercher dans la liste
* @return TRUE si {find} est dans la liste {lst}. FALSE sinon
*/
bool isInList( const std::vector<int8_t>& lst, int8_t find )
{
	for( auto e : lst )
		if( e == find )
			return true;
	return false;
}
