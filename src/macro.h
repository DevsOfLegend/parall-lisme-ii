#ifndef MACRO_h
#define MACRO_h
#include <stdio.h>
#include <stdlib.h>


#define assert( boolQuery, message, ... ) if( !(boolQuery) ){ fprintf(stderr, "[file "__FILE__", line %d] " message "\n", __LINE__, ##__VA_ARGS__); fflush(stderr); exit(__LINE__); }



/*******************************************************************************
* Système de debug
*/

// NOTE: utiliser fflush(stderr) pour afficher directe les données


/*!
 * @brief Permet d'afficher une erreur <b>SANS</b> EXIT
 * @param[in] msg	{String} Le message a afficher
 * @param[in] ...	{Std} Les arguments. (Cette fonction ce comporte comme printf)
 * @return[NONE]
 */
#define stdError( msg, ... ) {fprintf(stderr, "[file " __FILE__ ", line %d]: " msg "\n", __LINE__, ##__VA_ARGS__); fflush(stderr);}

/*!
 * @brief Permet d'afficher une erreur <b>AVEC</b> EXIT
 * @param[in] msg	{String} Le message a afficher
 * @param[in] ...	{Std} Les arguments. (Cette fonction ce comporte comme printf)
 * @return[NONE]
 */
#define stdErrorE( msg, ... ) {fprintf(stderr, "[file " __FILE__ ", line %d]: " msg "\n", __LINE__, ##__VA_ARGS__); fflush(stderr); exit(__LINE__);}


#endif// MACRO_h
